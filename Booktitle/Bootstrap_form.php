<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div >
    <h2 >BOOK Title -list</h2>
    </div>
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>serial</th>
            <th>ID</th>
            <th>BOOK Title</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>1213</td>
            <td>1</td>
            <td>C++</td>
           <td>  <button type="button" class="btn btn-primary btn-xs">ADD</button>
             <button type="button" STYLE="background-color: #ff4d4d " class="btn btn-primary btn-xs">DELETE</button>
             <button type="button" class="btn btn-primary btn-xs">TRASH</button>
             <button type="button" class="btn btn-primary btn-xs">UPDATE</button></td>
        </tr>
        <tr>
            <td>1214</td>
            <td>2</td>
            <td>JAVA</td>
            <td>  <button type="button" class="btn btn-primary btn-xs">ADD</button>
                <button type="button" STYLE="background-color: #ff4d4d " class="btn btn-primary btn-xs">DELETE</button>
                <button type="button" class="btn btn-primary btn-xs">TRASH</button>
                <button type="button" class="btn btn-primary btn-xs">UPDATE</button></td>
        </tr>
        <tr>
            <td>1215</td>
            <td>3</td>
            <td>DATA MINING</td>
            <td>  <button type="button" class="btn btn-primary btn-xs">ADD</button>
                <button type="button" STYLE="background-color: #ff4d4d " class="btn btn-primary btn-xs">DELETE</button>
                <button type="button" class="btn btn-primary btn-xs">TRASH</button>
                <button type="button" class="btn btn-primary btn-xs">UPDATE</button>

            </td>

        </tr>
        <tr>
            <td>1216</td>
            <td>4</td>
            <td>ALGORTIHM</td>
            <td>  <button type="button" class="btn btn-primary btn-xs">ADD</button>
                <button type="button" STYLE="background-color: #ff4d4d " class="btn btn-primary btn-xs">DELETE</button>
                <button type="button" class="btn btn-primary btn-xs">TRASH</button>
                <button type="button" class="btn btn-primary btn-xs">UPDATE</button></td>
        </tr>
        <tr>
            <td>1217</td>
            <td>5</td>
            <td>C</td>
            <td>  <button type="button" class="btn btn-primary btn-xs">ADD</button>
                <button type="button" STYLE="background-color: #ff4d4d " class="btn btn-primary btn-xs">DELETE</button>
                <button type="button" class="btn btn-primary btn-xs">TRASH</button>
                <button type="button" class="btn btn-primary btn-xs">UPDATE</button></td>
        </tr>
        <tr>
            <td>1218</td>
            <td>6</td>
            <td>NUMERICAL METHOD</td>
            <td>  <button type="button" class="btn btn-primary btn-xs">ADD</button>
                <button type="button" STYLE="background-color: #ff4d4d " class="btn btn-primary btn-xs">DELETE</button>
                <button type="button" class="btn btn-primary btn-xs">TRASH</button>
                <button type="button" class="btn btn-primary btn-xs">UPDATE</button></td>
        </tr>
        </tbody>
    </table>

<div class="col-sm-4">
<ul class="pagination">
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>

</div>
<div class="col-sm-8">
    <SELECT>

        <OPTION Value="SELECT">SELECT ITEM PER PAGE</OPTION>
        <OPTION Value="SELECT">10</OPTION>
        <OPTION Value="SELECT">20</OPTION>
        <OPTION Value="SELECT">30</OPTION>


    </SELECT>
</ul>
</div>
</div>
<div class="container">
    <button type="button" class="btn btn-primary">Add-New-Book-title</button>
    <button type="button" class="btn btn-success">View trash Items</button>
    <button type="button" class="btn btn-info">Download the list as PDF file</button>
    <button type="button" class="btn btn-info">Download the list as EXCEL file</button>

</div>
</body>
</html>
